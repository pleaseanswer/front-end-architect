## 2 注册命令

<img src="../img/核心流程_6.png" title="core模块——命令注册阶段" style="width: 680px" />

### 2.1 快速实现一个commander脚手架

```js
const commander = require('commander')
const pkg = require('../package.json')
```

* 方法一： 获取 `commander` 的单例
  * `const { program } = commander`

* 方法二： 实例化一个 `Command` 实例
  * `const program = new commander.Command()`

### 2.2 commander脚手架全局配置

```js
program
  .name(Object.keys(pkg.bin)[0])
  .usage('<command> [options]')
  .version(pkg.version)
  .option('-d, --debug', '是否开启调试模式', false)
  .option('-e, --envName <envName>', '获取环境变量名称')
  .parse(process.argv)
```

### 2.3 commander脚手架命令注册的两种方法

#### 1. command注册命令

```js
const clone = program.command('clone <source> [destination]')
clone
  .description('clone a repository')
  .option('-f, --force', '是否强制克隆')
  .action((source, destination, cmdObj) => {
  console.log('do clone', source, destination, cmdObj.force);
})
```

#### 2. addCommand注册命令

```js
const service = new commander.Command('service')
service
  .command('start [port]')
  .description('start service st some port')
  .action((port) => {
    console.log('do service start', port);
  })
service
  .command('stop')
  .description('stop service')
  .action(() => {
    console.log('stop service');
  })
program.addCommand(service)
```

### 2.4 commander的5种高级用法

#### 1. 通过arguments监听命令注册

```js
program
  .arguments('<cmd> [oprions]')
  .description('test command', {
    cmd: 'command to run',
    options: 'options for command'
  })
  .action((cmd, env) => {
    console.log(cmd, env);
  })
```

#### 2. command

```js
program
  .command('install [name]', 'install package')
  .alias('i')
```

```js
program
  .arguments('<cmd> [oprions]')
  .description('test command', {
    cmd: 'command to run',
    options: 'options for command'
  })
  .action((cmd, env) => {
    console.log(cmd, env);
  })
```

#### 3. 自定义help信息

```js
program.helpInformation = () => { 
  return ''
}
// EventImit
program.on('--help', () => {
  console.log('your help information');
})
```

#### 4. 实现debug模式

```js
program.on('option:debug', () => {
  if(program.debug) {
    program.env.LOG_LEVEL = 'verbose'
  }
  console.log(process.env.LOG_LEVEL);
})
```

#### 5. 对未知命令进行监听

```js
program.on('command:*', (obj) => {
  console.log(obj);
  console.error('未知的命令：' + obj[0]);
  const availableCommands = program.commands.map(cmd => cmd.name())
  console.log(availableCommands);
  console.log('可用命令：' + availableCommands.join(', '));
})
```
