## 3 Node项目如何支持ESModule

### 3.1 通过webpack完成ESModule资源构建

* webpack.config.js

> `commonjs` 规范

```js
const path = require('path')

module.exports = {
  entry: './bin/core.js',
  output: {
    path: path.join(__dirname, '/dist'),
    filename: 'core.js'
  },
  mode: 'development'
}
```

* package.json

```json
"scripts": {
  "build": "webpack",
  "dev": "webpack -w"
},
```

### 3.2 通过webpack target属性支持node内置库

```js
module.exports = {
  // ...
  target: 'node' // 默认 web
}
```

### 3.3 webpack loader配置babel-loader支持低版本node

* 安装依赖
  * `npm i -D babel-loader @babel/core @babel/preset-env @babel/plugin-transform-runtime @babel/runtime-corejs3`

```js
module: {
  rules: [
    {
      test: /\.js$/,
      exclude: /(node_modules|dist)/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env'],
          plugins: [
            [
              '@babel/plugin-transform-runtime',
              {
                corejs: 3,
                regenerator: true,
                useESModule: true,
                helpers: true
              }
            ]
          ]
        }
      }
    },
  ]
}
```

### 3.4 通过node原生支持ESModule

* 文件后缀 `.mjs`
* `node --experimental-modules bin/index.mjs`

