## 1 egg.js

### 1.1 初始化

* 初始化和项目启动方法

```bash
# 初始化
$ mkdir egg-example && cd egg-example
$ npm init egg --type=simple # 实际上执行的是 npm i create-egg
$ npm i
# 项目启动
$ npm run dev
$ open http://localhost:7001
```

### 1.2 通过 egg.js 框架添加新的 API

> app > controller > project.js

```js
'use strict';
const Controller = require('egg').Controller;
class ProjectController extends Controller {
  // 获取项目/组件的代码模板
  getTemplate() {
    const { ctx } = this;
    ctx.body =  'get template'
  }
}
module.exports = ProjectController;
```

> app > router.js

```js
'use strict';
// @param {Egg.Application} app - egg application
module.exports = app => {
  const { router, controller } = app;
  router.get('/project/template', controller.project.getTemplate);
};
```

## 2 云 mongodb

* [云 mongodb 开通](https://mongodb.console.aliyun.com/)
* [本地 mongodb 安装](https://www.runoob.com/mongodb/mongodb-tutorial.html)
* [mongodb 使用方法](https://www.runoob.com/mongodb/mongodb-databases-documents-collections.html)

### 2.1 本地 mongodb 调试技巧

#### 1. 启动

1. 在mongodb安装目录下新建mongo.config配置文件，内容如下：

```bash
  dbpath=D:\mongodb\data
  logpath=D:\mongodb\log\mongo.log
```

2. 在当前位置打开cmd命令行窗口
3. 执行命令：> mongod.exe --config mongo.config
4. 浏览器打开 `http://127.0.0.1:27017` -- 有如下显示说明启动成功

### 2.2 egg.js 接入 mongodb 方法

1. 创建 `mongo` 实例

> app > utils > mongo.js

```js
'use strict';

const Mongodb = require('@pick-star/cli-mongodb')
const { mongodbUrl, mongodbDbName } = require('../../config/db')

function mongo() {
  return new Mongodb(mongodbUrl, mongodbDbName)
}

module.exports = mongo
```

2. 设置配置数据

> config > db.js

```js
'use strict';

/** MONGODB **/
const mongodbUrl = 'mongodb://localhost:27017/zmoon-cli'
const mongodbDbName = 'zmoon-cli'

module.exports = {
  mongodbUrl,
  mongodbDbName
}
```

3. 服务器 api 执行

```js
'use strict';
const Controller = require('egg').Controller;
const mongo = require('../utils/mongo')

class ProjectController extends Controller {
  // 获取项目/组件的代码模板
  async getTemplate() {
    const { ctx } = this;
    const data = await mongo().query('project')
    ctx.body =  data
  }
}
module.exports = ProjectController;
```
