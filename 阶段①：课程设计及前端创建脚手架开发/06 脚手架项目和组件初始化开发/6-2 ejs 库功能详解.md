## 1 ejs 用法详解

### 1.1 ejs 模板的三种用法

```js
const ejs = require('ejs')

// 需要转义的字符串
const html = '<div><%= user.name %></div>'
const options = {}
// 替换的模板对象
const data = {
  user: {
    name: 'zmoon'
  }
}
```

#### 1. `let template = ejs.compile(str,options); template(data);` -- 输出渲染后的 HTML 字符串

* 适用于多次渲染

```js
// 返回compile function, 用于解析html中的ejs模板
// 主要消耗性能 -- compile
const template = ejs.compile(html, options)
// 编译后的template填入数据
const compiledTemplate = template(data)

console.log(compiledTemplate)
```

#### 2. `ejs.render(str, data, options);` -- 输出渲染后的 HTML 字符串

* 适用于一次性渲染

> = compile + 数据渲染

```js
const renderedTemplate = ejs.render(html, data, options)
console.log(renderedTemplate);
```

#### 3. `ejs.renderFile(filename, data, options, function(err, str) {}` -- 输出渲染后的 HTML 字符串

> = 读取文件 + render

* 用法一：`Promise`

```js
const renderedFile = ejs.renderFile(path.resolve(__dirname, 'template.html'), data, options)
renderedFile.then(file => console.log(file))
```

* 用法二： 回调

```js
ejs.renderFile(path.resolve(__dirname, 'template.html'), data, options, (err, file) => {
  console.log(file);
})
```

<img src="../img/ejs_1.png" />

### 1.2 标签含义

* `<%` '脚本' 标签，用于流程控制，无输出
* `<%_` 删除其前面的空格符
* `<%=` 输出数据到模板(输出是转义HTML标签)
* `<%-` 输出非转义的数据到模板
* `<%#` 注释标签，不执行、不输出内容
* `<%%` 输出字符串 '<%'
* `%>` 一般结束标签
* `-%>` 删除紧随其后的换行符
* `_%>` 将结束标签后面的空格符删除

#### 1. <% %> 脚本 标签

```bash
# template.html
<% if(user) { %>
  <div><%= user.name %></div>
<% } %>

# index.js
const callbackData = {
  user1: {
    name: 'zmoon'
  }
}
ejs.renderFile(path.resolve(__dirname, 'template.html'), callbackData, options, (err, file) => {
  console.log(file);
})

# 结果
undefined
```

```bash
# index.js
const callbackData = {
  user: {
    name: 'zmoon'
  }
}
ejs.renderFile(path.resolve(__dirname, 'template.html'), callbackData, options, (err, file) => {
  console.log(file);
})

# 结果

  <div>zmoon</div>

```

```jsp
<!-- template.html -->
<% if(user) { %>
  <% for(let i = 0; i < 10; i++) { %>
    <div><%= user.name %></div>
  <% } %>
<% } %>
```

#### 2. <%_ 删除其前面的空格符

```jsp
<%_ %><div><%= user.name %></div>
```

#### 3. <%= 输出数据到模板(输出是转义HTML标签)

```jsp
<div><%= user.name %></div>
```

#### 4. <%- 输出非转义的数据到模板

```bash
# index.js
const callbackData = {
  user: {
    nickName: '<div>zmoon<div>'
  }
}
ejs.renderFile(path.resolve(__dirname, 'template.html'), callbackData, options, (err, file) => {
  console.log(file);
})
# template.html
<%_ %><div><%= user.nickName %></div>
# 结果
<div>&lt;div&gt;zmoon&lt;div&gt;</div>
# template.html
<%_ %><%- user.nickName %>
# 结果
<div>zmoon<div>
```

#### 5. <%# 注释标签，不执行、不输出内容

* template.html

```jsp
<%# 输出用户的名称 %>
<%_ %><div><%= user.name %></div>
<%_ %><%- user.nickName %>
```

* 结果

```bash

<div>zmoon</div>
<div>zmoon<div>
```

#### 6. <%% 输出字符串 <%

```jsp
<%_ %><div><%= user.name %></div>
<%_ %><%- user.nickName %>
<%%
%%>
```

* 结果

```bash
<div>zmoon</div>
<div>zmoon<div>
<%
%>
```

#### 7. -%> 删除紧随其后的换行符

```jsp
<% if(user) { %>
  <%_ %><div><%= user.name %></div>
  <%_ %><%- user.nickName -%>
<% } %>
```

* 结果

```bash

<div>zmoon</div>
<div>zmoon<div>
```

#### 8. _%> 将结束标签后面的空格符删除

```jsp
<%_ %><div><%= user.name %></div>
<%_ %><%- user.nickName -%><% _%>
```

### 1.3 ejs 模板几种特殊用法

#### 1. 包含

```js
<!-- index.js -->
const callbackData = {
  user: {
    name: 'zmoon',
    nickName: '<div>zmoon<div>',
    copyright: 'zmoon'
  }
}
ejs.renderFile(path.resolve(__dirname, 'template.html'), callbackData, options, (err, file) => {
  console.log(file);
})
```

```jsp
<!-- template.html -->
<%- include('./footer.html', { user })-%>
<!-- footer.html -->
<div>footer copyright@<%= user.copyright%></div><% -%>
```

#### 2. 自定义分隔符

```js
const callbackData = {
  user: {
    name: 'zmoon',
    nickName: '<div>zmoon<div>',
    copyright: 'zmoon'
  }
}
const callbackOptions = {
  delimiter: '?'
}
ejs.renderFile(path.resolve(__dirname, 'template.html'), callbackData, callbackOptions, (err, file) => {
  console.log(file);
})
```

```jsp
<!-- template.html -->
<?- include('./footer.html', { user })-?>
<!-- footer.html -->
<div>footer copyright@<?= user.copyright?></div><? -?>
```

#### 3. 自定义文件加载器

```js
const callbackData = {
  user: {
    name: 'zmoon',
    nickName: '<div>zmoon<div>',
    copyright: 'zmoon'
  }
}
ejs.fileLoader = function(filePath) {
  const content = fs.readFileSync(filePath).toString()
  return '<div style="color: red;">from <%= user.copyright %></div>' + content
}
ejs.renderFile(path.resolve(__dirname, 'template.html'), callbackData, callbackOptions, (err, file) => {
  console.log(file);
})
```

```jsp
<!-- template.html -->
<%- include('./footer.html', { user })-%>
<!-- footer.html -->
<div>footer copyright@<%= user.copyright%></div><% -%>
<!-- 结果 -->
<div style="color: red;">from zmoon</div>

<div style="color: red;">from zmoon</div><div>footer copyright@zmoon</div>
```

## 2 glob 用法小结

### 2.1 匹配规则

* `*` 匹配任意 0 或多个任意字符
* `?` 匹配任意一个字符 
* `[...]` 若字符在中括号中，则匹配。若以 `!` 或 `^` 开头，若字符不在中括号中，则匹配
* `!(pattern|pattern|pattern)` 不满足括号中的所有模式则匹配
* `?(pattern/pattern|pattern)` 满足0或1括号中的模式则匹配
* `+(pattern|pattern pattern)` 满足1或 更多括号中的模式则匹配
* `*(alb/c)` 满足0或 更多括号中的模式则匹配
* `@(pattern|pat*|pat?erN)` 满足1个括号中的模式则匹配
* `**` 跨路径匹配任意字符

```js
const glob = require('glob')

// 文件遍历
glob('**/*.js', {
  ignore: ['node_modules/**', 'webpack.config.js']
}, function(err, file) {
  console.log(err, file);
})
```
