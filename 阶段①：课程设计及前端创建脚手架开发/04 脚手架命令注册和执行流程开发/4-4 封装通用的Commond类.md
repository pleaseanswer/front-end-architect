## 1 脚手架命令动态加载功能架构设计

<img src="../img/高性能脚手架架构设计_2.png" />

### 1.1 通用脚手架命令 Command 类封装

#### 1. commander 脚手架初始化

* models>command>lib>index.js

```js
class Command {
  constructor() {}
  init() {} // 准备阶段
  exec() {} // 执行阶段

}
module.exports = Command
```

#### 2. 动态加载 initCommand + new initCommand

* commands>init>lib>index.js

```js
const Command = require('@zmoon-cli-dev/command')
class InitCommand extends Command {}
function init() {
  return new InitCommand()
}
module.exports = init
module.exports.InitCommand = InitCommand
```

#### 3. Command constructor

* commands>init>lib>index.js

```js
const Command = require('@zmoon-cli-dev/command')
class InitCommand extends Command {}
function init(argv) {
  return new InitCommand(argv)
}
module.exports = init
module.exports.InitCommand = InitCommand
```

* models>command>lib>index.js

```js
class Command {
  constructor(argv) {
    console.log('Commond constructor', argv);
    this._argv = argv
    let runner = new Promise((resolve, reject) => {
      let chain = Promise.resolve()
      chain = chain.then(() => {})
    })
  }
  init() {
    throw new Error('init 必须实现')
  }
  exec() {
    throw new Error('init 必须实现')
  }
}
```

#### 4. 命令的准备阶段 -- 检查 node 版本

* models>command>lib>index.js

```js
const semver = require('semver')
const colors = require('colors/safe')
const log = require('@zmoon-cli-dev/log')

const LOWEST_NODE_VERSION = '12.0.0'

class Command {
  constructor(argv) {
    this._argv = argv
    let runner = new Promise((resolve, reject) => {
      let chain = Promise.resolve()
      chain = chain.then(() => this.checkNodeVersion())
      chain.catch(err => {
        log.error(err.message);
      })
    })
  }
  // 检查node版本
  checkNodeVersion() {
    // 1. 获取当前 node 版本号
    const currentVersion = process.version
    // 2. 比对最低版本号
    const lowestNodeVersion = LOWEST_NODE_VERSION
    if (!semver.gte(currentVersion, lowestNodeVersion)) {
      throw new Error(colors.red(`imooc-cli 需要安装v${lowestNodeVersion}以上版本的node.js`))
    }
  }
}
```

#### 5. 命令的准备阶段 -- 参数初始化

> 通过异步执行的命令都需要单独进行错误捕获
> 即每次新建 `promise` 都需要有单独的 `catch`

* models>command>lib>index.js

```js
class Command {
  constructor(argv) {
    // console.log('Commond constructor', argv);
    if(!argv) {
      throw new Error('参数不能为空！')
    }
    if(!Array.isArray(argv)) {
      throw new Error('参数必须为数组！')
    }
    if(argv.length < 1) {
      throw new Error('参数列表为空！')
    }
    this._argv = argv
    let runner = new Promise((resolve, reject) => {
      let chain = Promise.resolve()
      chain = chain.then(() => this.checkNodeVersion())
      chain = chain.then(() => this.initArgs())
      chain = chain.then(() => this.init())
      chain = chain.then(() => this.exec())
      chain.catch(err => {
        log.error(err.message);
      })
    })
  }
  initArgs() {
    this.cmd = this._argv[this._argv.length-1]
    this._argv = this._argv.slice(0, this._argv.length-1)
  }
  // ...
  init() {
    throw new Error('init 必须实现！')
  }
  exec() {
    throw new Error('exec 必须实现！')
  }
}
```

* commands>init>lib>index.js

```js
class InitCommand extends Command {
  init() {
    this.projectName = this._argv[0] || ''
    this.force = !!this.cmd._optionValues.force
    log.verbose('projectName', this.projectName);
    log.verbose('force', this.force);
  }
  exec() {}
}
```
