## 1 脚手架命令动态加载功能架构设计

<img src="../img/高性能脚手架架构设计_3.png" />

### 1.1 指定本地调试文件路径targetPath

* core>cli>bin>index.js

```js
function registerCommand() {
  program
    .name(Object.keys(pkg.bin)[0])
    .usage('<command> [options]')
    .version(pkg.version)
    .option('-d, --debug', '是否开启调试模式', false)
    .option('-tp, --targetPath <targetPath>', '是否指定本地调试文件路径', '')

  program
    .command('init [projectName]')
    .option('-f, --force', '是否强制初始化项目')
    .action(exec)

  // 指定targetPath
  program.on('option:targetPath', function() {
    process.env.CLI_TARGET_PATH = program._optionValues.targetPath
  })

  program.parse(process.argv)
  if(program.args && program.args.length < 1) {
    program.outputHelp()
  }
}
```

* commands>init>lib>index.js

```js
function init(projectName, cmdObj) {
  console.log('init', projectName, cmdObj.force, process.env.CLI_TARGET_PATH);
}
```

### 1.2 动态执行库exec模块创建

> lerna create exec

```js
function exec() {
  console.log('targetPath', targetPath)
  console.log('homePath', homePath)
}
module.exports = exec;
```

### 1.3 创建npm模块通用类Package

* models>package>lib>index.js

```js
'use strict';

class Package {
  constructor(options) {
  }
}

module.exports = Package;
```

* core>exec>lib>index.js

```js
const Package = require('@zmoon-cli-dev/package')
const log = require('@zmoon-cli-dev/log')
function exec() {
  const targetPath = process.env.CLI_TARGET_PATH
  const homePath = process.env.CLI_HOME_PATH
  log.verbose('targetPath', targetPath)
  log.verbose('homePath', homePath)
  const pkg = new Package({
      targetPath
  })
  console.log(pkg);
}
module.exports = exec;
```

### 1.4 Package类的属性、方法定义及构造函数

#### 1. Package类的属性、方法定义

```js
class Package {
  constructor(options) {
    // package的路径
    this.targetPath = options.targetPath
    // package的存储路径
    this.storePath = options.storePath
    // package的name
    this.packageName = options.name
    // package的version
    this.packageVersion = options.version
  }
  // 判断当前Package是否存在
  exists() {}
  // 安装Package
  install() {}
  // 更新Package
  update() {}
  // 获取入口文件的路径
  getRootFilePath() {}
}
```

#### 2. Package构造函数逻辑开发

* models>package>lib>index.js

```js
const { isObject } = require('@zmoon-cli-dev/utils') 
class Package {
  constructor(options) {
    if(!options) {
      throw new Error('Package类的options参数不能为空！')
    }
    if(!isObject(options)) {
      throw new Error('Package类的options参数必须为对象！')
    }
    // package的路径
    this.targetPath = options.targetPath
    // package的存储路径
    this.storePath = options.storePath
    // package的name
    this.packageName = options.packageName
    // package的version
    this.packageVersion = options.packageVersion
  }
}
```

* utils>utils>lib>index.js

```js
function isObject(o) {
  return Object.prototype.toString.call(o) === '[object Object]'
}
```

* core>exec>lib>index.js

```js
const SETTINGS = {
  init: '@zmoon-cli-dev/init'
}

function exec() {
  const targetPath = process.env.CLI_TARGET_PATH
  const homePath = process.env.CLI_HOME_PATH

  const cmdObj = arguments[arguments.length-1]
  const cmdName = cmdObj.name()
  const packageName = SETTINGS[cmdName]
  const packageVersion = 'latest'

  const pkg = new Package({
    targetPath,
    packageName,
    packageVersion
  })
}
```

### 1.5 Package类获取文件入口路径

> pkg-dir应用+解决不同操作系统路径兼容问题

#### 1. 获取package.json所在目录 -- pkg-dir

```js
const pkgDir = require('pkg-dir').sync
getRootFilePath() {
  const dir = pkgDir(this.targetPath)
}
```

#### 2. 读取package.json -- require() js/json/node

```js
const pkgFile = require(path.resolve(dir, 'package.json'))
```

#### 3. 寻找main/lib -- path

```js
if(pkgFile && pkgFile.main) {}
```

#### 4. 路径的兼容（macOS/windows）

```js
if(pkgFile && pkgFile.main) {
  return formatPath(path.resolve(dir, pkgFile.main))
}
```

* utils>format-path>lib>index.js

```js
const path = require('path')

function formatPath(p) {
  if(p && typeof p === 'string') {
    // 分隔符
    const sep = path.sep
    if(sep === '/') {
      return p.replace(/\//g, '\\')
    } else {
      return p
    }
  }
  return p
}
```

### 1.6 利用npminstall库安装npm模块

* models>package>lib>index.js

```js
const npminstall = require('npminstall')
const { getDefaultRegistry } = require('@zmoon-cli-dev/get-npm-info')
// 安装Package
install() {
  return npminstall({
    root: this.targetPath, // 模块路径
    storeDir: this.storeDir,
    registry: getDefaultRegistry(),
    pkgs: [{
      name: this.packageName,
      version: this.packageVersion
    }]
  })
}
```

* utils>get-npm-info>lib>index.js

```js
function getDefaultRegistry(isOriginal = false) {
  return isOriginal ? 'https://registry.npmjs.org' : 'https://registry.npm.taobao.org'
}
```

* core>exec>lib>index.js

```js
const SETTINGS = {
  init: '@zmoon-cli-dev/init'
}
const CACHE_DIR = 'dependencies'

function exec() {
  let targetPath = process.env.CLI_TARGET_PATH
  const homePath = process.env.CLI_HOME_PATH
  let storeDir = ''
  let pkg

  const cmdObj = arguments[arguments.length-1]
  const cmdName = cmdObj.name()
  const packageName = SETTINGS[cmdName]
  const packageVersion = 'latest'

  if(!targetPath) {
    targetPath = path.resolve(homePath, CACHE_DIR) // 生成缓存路径
    storeDir = path.resolve(targetPath, 'node_modules')
    pkg = new Package({
      targetPath,
      storeDir,
      packageName,
      packageVersion
    })
    if(pkg.exists()) {
      // 更新Package
    } else {
      // 安装Package
      await pkg.install()
    }
  } else {
    pkg = new Package({
      targetPath,
      packageName,
      packageVersion
    })
  }
  const rootFile = pkg.getRootFilePath()
  if(rootFile) {
    require(rootFile).apply(null, arguments) // [] -> 参数列表
  }
}
```

### 1.7 Package类判断模块是否存在

* models>package>lib>index.js

```js
// package的缓存目录前缀
// this.cacheFilePathPrefix = this.packageName.replace('/', '_')
async exists() {
  if(this.storeDir) {
    await this.prepare()
    return pathExists(this.cacheFilePath)
  } else {
    return pathExists(this.targetPath)
  }
}
async prepare() {
  if(this.packageVersion === 'latest') {
    this.packageVersion = await getNpmLatestVersion(this.packageName)
  }
}

// @imooc-cli/init 1.1.2 -> _@imooc-cli_init@1.1.2@@imooc-cli
get cacheFilePath() {
  return path.resolve(this.storeDir, `_${this.cacheFilePathPrefix}@${this.packageVersion}@${this.packageName}`)
}
```

* utils>get-npm-info>lib>index.js

```js
async function getNpmLatestVersion(npmName, registry) {
  const version = await getNpmVersions(npmName, registry)
  if(version) {
    // return version.sort((a, b) => semver.gt(b, a))[0]
    return version[version.length-1]
  }
  return null
}
```

### 1.8 Package类更新模块

* models>package>lib>index.js

```js
// 最新版本存在则不需要更新
async update() {
  await this.prepare()
  // 1. 获取最新的npm模块版本号
  const latestPackageVersion = await getNpmLatestVersion(this.packageName)
  // 2. 查询最新版本号对应的路径是否存在
  const latestFilePath = this.getSpecificCacheFilePath(latestPackageVersion)
  // 3. 如果不存在，则直接安装最新版本
  if(!pathExists(latestFilePath)) {
    return npminstall({
      root: this.targetPath, // 模块路径
      storeDir: this.storeDir,
      registry: getDefaultRegistry(),
      pkgs: [{
        name: this.packageName,
        version: latestPackageVersion
      }]
    })
  }
  return latestFilePath
}
async prepare() {
  if(this.storeDir && !pathExists(this.storeDir)) {
    fse.mkdirSync(this.storeDir) // 将当前路径不存在的文件都创建
  }
  if(this.packageVersion === 'latest') {
    this.packageVersion = await getNpmLatestVersion(this.packageName)
  }
}
getSpecificCacheFilePath(packageVersion) {
  return path.resolve(this.storeDir, `_${this.cacheFilePathPrefix}@${packageVersion}@${this.packageName}`)
}
```

### 1.9 Package类获取缓存模块入口文件功能改造

* models>package>lib>index.js

```js
// 获取入口文件的路径
getRootFilePath() {
  function _getRootFile(targetPath) {
    // 1. 获取package.json所在目录 -- pkg-dir
    const dir = pkgDir(targetPath)
    if(dir) {
      // 2. 读取package.json -- require() js/json/node
      const pkgFile = require(path.resolve(dir, 'package.json'))
      // 3. 寻找main/lib -- path
      if(pkgFile && pkgFile.main) {
        // 4. 路径的兼容（macOS/windows）
        return formatPath(path.resolve(dir, pkgFile.main))
      }
    }
    return null
  }
  if(this.storeDir) {
    return _getRootFile(this.cacheFilePath)
  } else {
    return _getRootFile(this.targetPath)
  }
}
```

