### 1 脚手架架构设计和框架搭建

#### 1.1 脚手架执行原理

1. 在终端输入 `vue create vue-test-app`
2. 终端解析出 `vue` 命令
3. 终端在环境变量中找到 `vue` 命令
4. 终端根据 `vue` 命令链接到实际文件 `vue.js`
5. 终端利用 `node` 执行 `vue.js`
6. `vue.js` 解析 `command/options`
7. `vue.js` 执行 `command`
8. 执行完毕，退出执行

#### 1.2 脚手架管理工具 lerna 开发脚手架流程

1. 脚手架项目初始化 `lerna init`
2. 创建 `package`
	* `lerna create`
	* `lerna add` 安装依赖
	* `lerna link` 链接依赖
3. 脚手架开发和测试 `lerna run`
4. 脚手架发布上线 `lerna publish`

### 2 脚手架核心流程开发

#### 2.1 脚手架准备阶段过程开发

1. 检查版本号
2. 检查 `node` 版本
3. `root` 账号启动检查和自动降级
4. 用户主目录检查
5. 入参检查
6. 环境变量检查
7. 检查是否为最新版本
8. 运用 `npm API` 实现脚手架自动更新

### 3 脚手架命令注册和执行流程开发

#### 3.1 高性能脚手架架构优化

* 动态加载
* 使用缓存
* 动态加载完使用 `node` 多进程执行

#### 3.2 脚手架命令动态加载功能架构设计

##### 封装通用的 Package 类

1. 指定本地调试文件路径 `targetPath`
2. 动态执行库 `exec` 模块创建 【`core`】
3. 创建 `npm` 模块通用类 `Package` 【`models`]】
4. `Package` 类的属性、方法定义及构造函数 【`utils`】
5. `Package` 类获取文件入口路径
	* 获取 `package.json` 所在目录 -- `pkg-dir`
	* 读取 `package.json` -- `require()`
	* 寻找 `main/lib` -- `path`
	* 路径的兼容（`macOS/windows`）【`utils`】
6. 利用 `npminstall` 库安装 `npm` 模块 【`models` `utils`】
7. `Package` 类判断模块是否存在
8. `models` 【`utils`】
9. `Package` 类更新模块 【`models`】
10. `Package` 类获取缓存模块入口文件功能改造 【`models`】

##### 封装通用的 Command 类

1. `commander` 脚手架初始化
	* `constructor()` + `init()` + `exec()`
2. 动态加载 `initCommand` + `new initCommand`
	* `class initCommand extends Command {}` 
3. `Command constructor`
	* `this._argv` + `run()` 
4. 命令的准备阶段
	* 检查 `node` 版本
	* 参数初始化
